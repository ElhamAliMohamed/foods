from django.shortcuts import render
import requests
from django.http import JsonResponse
from pymongo import MongoClient
from rest_framework import status

# Create your views here.
def food(request):
    client = MongoClient('localhost', 27017)
    # creating database
    db = client['foodegy']
    # creating acollection
    collec = db['food']
    brand_names = list(collec.distinct('Food'))

    if 'foodname' in request.GET:

        foodname = request.GET['foodname']
        a = foodname.capitalize()
        foodname = a or request.GET['foodname']


        if foodname in brand_names:
            cursor = collec.find_one({'Food': foodname})
            try:
                food = cursor["Food"]
                measure = cursor["Measure"]
                grams = cursor["Grams"]
                calories = cursor["Calories"]
                protein = cursor["Protein"]
                fat = cursor["Fat"]
                fiber = cursor["Fiber"]
                carbs = cursor["Carbs"]
                category = cursor["Category"]
                foods ={
                    "food": food,
                    "measure": measure,
                    "grams": grams,
                    "calories": calories,
                    "protein": protein,
                    "fat": fat,
                    "fiber":fiber,
                    "carbs":carbs,
                    "category":category,
                }
                return render(request, 'food.html', {'foods': foods})
            except(TypeError, KeyError):
                return JsonResponse({"message": "error500"}, status=status.HTTP_200_OK, safe=False)
        else:
            return JsonResponse({"message": "Food Not exist"}, status=status.HTTP_200_OK, safe=False)



    else:

        return render(request, 'food.html')
